"""Module is an example of a simple call to the Wikimedia REST API.
"""

import os
import json
from dotenv import load_dotenv
import requests

ARTICLE_TITLE = "Banana"
LANGUAGE = "en"

load_dotenv()
access_token = os.getenv("ACCESS_TOKEN")

url = f"https://api.enterprise.wikimedia.com/v2/articles/{ARTICLE_TITLE}"
headers = {
    "Authorization": f"Bearer {access_token}",
    "Content-Type": "application/json",
}

body = {
    "fields": ["name", "identifier"],
    "filters": [{"field": "in_language.identifier", "value": LANGUAGE}],
    "limit": 3,
}

response = requests.get(url, headers=headers, data=json.dumps(body), timeout=5)


print(json.dumps(response.json(), indent=4))
