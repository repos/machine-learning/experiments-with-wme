"""Module is an example of a call to get the list of available languages
from the Wikimedia Enterprise REST API.
"""

import os
from dotenv import load_dotenv
import requests

load_dotenv()
access_token = os.getenv("ACCESS_TOKEN")

URL = "https://api.enterprise.wikimedia.com/v2/languages"
headers = {"Authorization": f"Bearer {access_token}"}

response = requests.get(URL, headers=headers, timeout=5)

print(response.json())
